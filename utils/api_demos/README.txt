To enable stereo rendering, use option -s :      mainGlut -s toto.pos

Key functions are :

 Ctrl+Q                Quit
 1                    mesh line (si vous partez d'un *.geo)
 2                    mesh surface (si vous partez d'un *.geo)
 3                    mesh volume (si vous partez d'un *.geo)
 
 R                    resize
 0                    origine (paramètres d'usine)
 
 F                    full screen on
 ech                  full screen off

 D                    displacement field (si vous partez d'un *.pos)
 +                    deformed +(augmente le facteur de deformation)
 -                    deformed - (reduit le facteur de deformation)

Advanced options :
  left                eye sep -  
  right               eye sep +  
  up                  focal length + 
  down                focal length -  

