// Gmsh - Copyright (C) 1997-2012 C. Geuzaine, J.-F. Remacle
//
// See the LICENSE.txt file for license information. Please report all
// bugs and problems to <gmsh@geuz.org>.

#ifndef _GSHHS_H_
#define _GSHHS_H_

#include "Plugin.h"

extern "C"
{
  GMSH_Plugin *GMSH_RegisterGSHHSPlugin();
}

#endif
