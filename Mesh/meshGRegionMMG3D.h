// Gmsh - Copyright (C) 1997-2012 C. Geuzaine, J.-F. Remacle
//
// See the LICENSE.txt file for license information. Please report all
// bugs and problems to <gmsh@geuz.org>.

#ifndef _MESHGREGIONMMG3D_H_
#define _MESHGREGIONMMG3D_H_

class GRegion;

void refineMeshMMG(GRegion *gr);

#endif
