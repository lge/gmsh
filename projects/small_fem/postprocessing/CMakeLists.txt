set(src
  Writer.cpp
  WriterDummy.cpp
  WriterMsh.cpp
  WriterVector.cpp

  Solution.cpp
  PlotBasis.cpp

  Integrator.cpp
)


add_sources(postprocessing "${src}")
