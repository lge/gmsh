#include "Exception.h"
#include "fullMatrix.h"
#include "GaussIntegration.h"
#include "Polynomial.h"
#include "Mapper.h"

#include "FormulationSteadyWave.h"

using namespace std;

// Pi  = atan(1) * 4
// Mu  = 4 * Pi * 10^-7
// Eps = 8.85418781762 * 10^−12
//const double FormulationSteadyWave::mu  = 4 * atan(1) * 4 * 1E-7;
//const double FormulationSteadyWave::eps = 8.85418781762E-12;

const double FormulationSteadyWave::mu  = 1;
const double FormulationSteadyWave::eps = 1;

FormulationSteadyWave::FormulationSteadyWave(const GroupOfElement& goe,
					     double k,
					     unsigned int order){
  // Gaussian Quadrature Data (Term One) // 
  // NB: We need to integrad a rot * rot !
  //     and order(rot f) = order(f) - 1
  gC1 = new fullMatrix<double>();
  gW1 = new fullVector<double>();

  // Look for 1st element to get element type
  // (We suppose only one type of Mesh !!)
  gaussIntegration::get(goe.get(0).getType(), (order - 1) + (order - 1) + 2 , *gC1, *gW1);

  G1 = gW1->size(); // Nbr of Gauss points

  // Gaussian Quadrature Data (Term Two) //
  // NB: We need to integrad a f * f !
  gC2 = new fullMatrix<double>();
  gW2 = new fullVector<double>();

  // Look for 1st element to get element type
  // (We suppose only one type of Mesh !!)
  gaussIntegration::get(goe.get(0).getType(), order + order + 2, *gC2, *gW2);

  G2 = gW2->size(); // Nbr of Gauss points

  
  // Wave Number Squared //
  kSquare = k * k;

  // Function Space //
  fspace = new FunctionSpaceEdge(goe, order);
}

FormulationSteadyWave::~FormulationSteadyWave(void){
  delete gC1;
  delete gW1;
  delete gC2;
  delete gW2;
  delete fspace;
}

double FormulationSteadyWave::weak(int dofI, int dofJ,
				   const GroupOfDof& god) const{
  // Init Some Stuff //
  fullVector<double> curlPhiI(3);
  fullVector<double> curlPhiJ(3);
  fullVector<double> phiI(3);
  fullVector<double> phiJ(3);

  fullMatrix<double> jac(3, 3);        
  fullMatrix<double> invJac(3, 3);       

  double integral1 = 0;
  double integral2 = 0;
  double det;

  // Get Element and Basis Functions (+ Curl) //
  const MElement& element = god.getGeoElement();
  MElement&      celement = const_cast<MElement&>(element);
  
  const vector<const vector<Polynomial>*> curlFun = 
    fspace->getCurlLocalFunctions(element);

  const vector<const vector<Polynomial>*> fun = 
    fspace->getLocalFunctions(element);

  // Loop over Integration Point (Term 1) //
  for(int g = 0; g < G1; g++){
    det = celement.getJacobian((*gC1)(g, 0), 
			       (*gC1)(g, 1), 
			       (*gC1)(g, 2), 
			       jac);
    
    curlPhiI = Mapper::curl(Polynomial::at(*curlFun[dofI], 
					   (*gC1)(g, 0), 
					   (*gC1)(g, 1),
					   (*gC1)(g, 2)),
			    jac, 1 / det);
    
    curlPhiJ = Mapper::curl(Polynomial::at(*curlFun[dofJ], 
					   (*gC1)(g, 0), 
					   (*gC1)(g, 1), 
					   (*gC1)(g, 2)),
			    jac, 1 / det);
    
    integral1 += 
      ((curlPhiI * curlPhiJ) / mu) * fabs(det) * (*gW1)(g);
  }


  // Loop over Integration Point (Term 2) //
  for(int g = 0; g < G2; g++){
    det = celement.getJacobian((*gC2)(g, 0), 
			       (*gC2)(g, 1), 
			       (*gC2)(g, 2), 
			       invJac);

    invJac.invertInPlace();

    phiI = Mapper::grad(Polynomial::at(*fun[dofI],
				       (*gC2)(g, 0), 
				       (*gC2)(g, 1),
				       (*gC2)(g, 2)),
			invJac);
    
    phiJ = Mapper::grad(Polynomial::at(*fun[dofJ],
				       (*gC2)(g, 0), 
				       (*gC2)(g, 1),
				       (*gC2)(g, 2)),
			invJac);
    
    integral2 += 
      ((phiI * phiJ) * eps * kSquare) * fabs(det) * (*gW2)(g);
  }

  return integral1 - integral2;
}
