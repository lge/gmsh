@c
@c This file is generated automatically by running "gmsh -doc".
@c Do not edit by hand!
@c

@ftable @code
@item Plugin(AnalyseCurvedMesh)
Plugin(AnalyseCurvedMesh) check the jacobian of all elements of dimension 'Dim' or the greater model dimension if 'Dim' is either <0 or >3.

 Analysis : 0 do nothing
          +1 find invalid elements (*)
          +2 compute J_min and J_max of all elements and print some statistics

Effect (for *) : 0 do nothing
                +1 print a list of invalid elements
                +2 print some statistics
                +4 hide valid elements (for GUI)

MaxDepth = 0,1,...
         0 : only sample the jacobian
         1 : compute Bezier coefficients
        2+ : execute a maximum of 1+ subdivision(s)

JacBreak = [0,1[ : if a value of the jacobian <= 'JacBreak' is found, the element is said to be invalid

BezBreak = [0,JacBreak[ : if all Bezier coefficients are > 'BezBreak', the element is said to be valid

Tolerance = R+ , << 1 : tolerance (relatively to J_min and J_max) used during the computation of J_min and J_max
Numeric options:
@table @code
@item Dim
Default value: @code{-1}
@item Analysis
Default value: @code{2}
@item Effect (1)
Default value: @code{6}
@item JacBreak (1)
Default value: @code{0}
@item BezBreak (1)
Default value: @code{0}
@item MaxDepth (1,2)
Default value: @code{20}
@item Tolerance (2)
Default value: @code{0.001}
@end table

@item Plugin(Annotate)
Plugin(Annotate) adds the text string `Text', in font `Font' and size `FontSize', in the view `View'. The string is aligned according to `Align'.

If `ThreeD' is equal to 1, the plugin inserts the string in model coordinates at the position (`X',`Y',`Z'). If `ThreeD' is equal to 0, the plugin inserts the string in screen coordinates at the position (`X',`Y').

If `View' < 0, the plugin is run on the current view.

Plugin(Annotate) is executed in-place for list-based datasets or creates a new view for other datasets.
String options:
@table @code
@item Text
Default value: @code{"My Text"}
@item Font
Default value: @code{"Helvetica"}
@item Align
Default value: @code{"Left"}
@end table
Numeric options:
@table @code
@item X
Default value: @code{50}
@item Y
Default value: @code{30}
@item Z
Default value: @code{0}
@item ThreeD
Default value: @code{0}
@item FontSize
Default value: @code{14}
@item View
Default value: @code{-1}
@end table

@item Plugin(Bubbles)
Plugin(Bubbles) constructs a geometry consisting of `bubbles' inscribed in the Voronoi of an input triangulation. `ShrinkFactor' allows to change the size of the bubbles. The plugin expects a triangulation in the `z = 0' plane to exist in the current model.

Plugin(Bubbles) creates one `.geo' file.
String options:
@table @code
@item OutputFile
Default value: @code{"bubbles.geo"}
@end table
Numeric options:
@table @code
@item ShrinkFactor
Default value: @code{0}
@end table

@item Plugin(Curl)
Plugin(Curl) computes the curl of the field in the view `View'.

If `View' < 0, the plugin is run on the current view.

Plugin(Curl) creates one new view.
Numeric options:
@table @code
@item View
Default value: @code{-1}
@end table

@item Plugin(CutBox)
Plugin(CutBox) cuts the view `View' with a rectangular box defined by the 4 points (`X0',`Y0',`Z0') (origin), (`X1',`Y1',`Z1') (axis of U), (`X2',`Y2',`Z2') (axis of V) and (`X3',`Y3',`Z3') (axis of W).

The number of points along U, V, W is set with the options `NumPointsU', `NumPointsV' and `NumPointsW'.

If `ConnectPoints' is zero, the plugin creates points; otherwise, the plugin generates hexahedra, quadrangles, lines or points depending on the values of `NumPointsU', `NumPointsV' and `NumPointsW'.

If `Boundary' is zero, the plugin interpolates the view inside the box; otherwise the plugin interpolates the view at its boundary.

If `View' < 0, the plugin is run on the current view.

Plugin(CutBox) creates one new view.
Numeric options:
@table @code
@item X0
Default value: @code{0}
@item Y0
Default value: @code{0}
@item Z0
Default value: @code{0}
@item X1
Default value: @code{1}
@item Y1
Default value: @code{0}
@item Z1
Default value: @code{0}
@item X2
Default value: @code{0}
@item Y2
Default value: @code{1}
@item Z2
Default value: @code{0}
@item X3
Default value: @code{0}
@item Y3
Default value: @code{0}
@item Z3
Default value: @code{1}
@item NumPointsU
Default value: @code{20}
@item NumPointsV
Default value: @code{20}
@item NumPointsW
Default value: @code{20}
@item ConnectPoints
Default value: @code{1}
@item Boundary
Default value: @code{1}
@item View
Default value: @code{-1}
@end table

@item Plugin(CutGrid)
Plugin(CutGrid) cuts the view `View' with a rectangular grid defined by the 3 points (`X0',`Y0',`Z0') (origin), (`X1',`Y1',`Z1') (axis of U) and (`X2',`Y2',`Z2') (axis of V).

The number of points along U and V is set with the options `NumPointsU' and `NumPointsV'.

If `ConnectPoints' is zero, the plugin creates points; otherwise, the plugin generates quadrangles, lines or points depending on the values of `NumPointsU' and `NumPointsV'.

If `View' < 0, the plugin is run on the current view.

Plugin(CutGrid) creates one new view.
Numeric options:
@table @code
@item X0
Default value: @code{0}
@item Y0
Default value: @code{0}
@item Z0
Default value: @code{0}
@item X1
Default value: @code{1}
@item Y1
Default value: @code{0}
@item Z1
Default value: @code{0}
@item X2
Default value: @code{0}
@item Y2
Default value: @code{1}
@item Z2
Default value: @code{0}
@item NumPointsU
Default value: @code{20}
@item NumPointsV
Default value: @code{20}
@item ConnectPoints
Default value: @code{1}
@item View
Default value: @code{-1}
@end table

@item Plugin(CutParametric)
Plugin(CutParametric) cuts the view `View' with the parametric function (`X'(u), `Y'(u), `Z'(u)), using `NumPointsU' values of the parameter u in [`MinU', `MaxU'].

If `ConnectPoints' is set, the plugin creates line elements; otherwise, the plugin generates points.

If `View' < 0, the plugin is run on the current view.

Plugin(CutParametric) creates one new view.
String options:
@table @code
@item X
Default value: @code{"0.1 + 0.5 * Cos(u)"}
@item Y
Default value: @code{"0.1 + 0.5 * Sin(u)"}
@item Z
Default value: @code{"0"}
@end table
Numeric options:
@table @code
@item MinU
Default value: @code{0}
@item MaxU
Default value: @code{6.2832}
@item NumPointsU
Default value: @code{360}
@item ConnectPoints
Default value: @code{0}
@item View
Default value: @code{-1}
@end table

@item Plugin(CutPlane)
Plugin(CutPlane) cuts the view `View' with the plane `A'*X + `B'*Y + `C'*Z + `D' = 0.

If `ExtractVolume' is nonzero, the plugin extracts the elements on one side of the plane (depending on the sign of `ExtractVolume').

If `View' < 0, the plugin is run on the current view.

Plugin(CutPlane) creates one new view.
Numeric options:
@table @code
@item A
Default value: @code{1}
@item B
Default value: @code{0}
@item C
Default value: @code{0}
@item D
Default value: @code{-0.01}
@item ExtractVolume
Default value: @code{0}
@item RecurLevel
Default value: @code{4}
@item TargetError
Default value: @code{0}
@item View
Default value: @code{-1}
@end table

@item Plugin(CutSphere)
Plugin(CutSphere) cuts the view `View' with the sphere (X-`Xc')^2 + (Y-`Yc')^2 + (Z-`Zc')^2 = `R'^2.

If `ExtractVolume' is nonzero, the plugin extracts the elements inside (if `ExtractVolume' < 0) or outside (if `ExtractVolume' > 0) the sphere.

If `View' < 0, the plugin is run on the current view.

Plugin(CutSphere) creates one new view.
Numeric options:
@table @code
@item Xc
Default value: @code{0}
@item Yc
Default value: @code{0}
@item Zc
Default value: @code{0}
@item R
Default value: @code{0.25}
@item ExtractVolume
Default value: @code{0}
@item RecurLevel
Default value: @code{4}
@item TargetError
Default value: @code{0}
@item View
Default value: @code{-1}
@end table

@item Plugin(DiscretizationError)
Plugin(DiscretizationError) computes the error between the mesh and the geometry. It does so by supersampling the elements and computing the distance between the supersampled points dans their projection on the geometry.
Numeric options:
@table @code
@item SuperSamplingNodes
Default value: @code{10}
@end table

@item Plugin(Distance)
Plugin(Distance) computes distances to physical entities in a mesh.

Define the physical entities to which the distance is computed. If Point=0, Line=0, and Surface=0, then the distance is computed to all the boundaries of the mesh (edges in 2D and faces in 3D).

Computation<0. computes the geometrical euclidian distance (warning: different than the geodesic distance), and  Computation=a>0.0 solves a PDE on the mesh with the diffusion constant mu = a*bbox, with bbox being the max size of the bounding box of the mesh (see paper Legrand 2006).

Min Scale and max Scale, scale the distance function. If min Scale<0 and max Scale<0, then no scaling is applied to the distance function.

Plugin(Distance) creates a new distance view and also saves the view in the fileName.pos file.
String options:
@table @code
@item Filename
Default value: @code{"distance.pos"}
@end table
Numeric options:
@table @code
@item PhysPoint
Default value: @code{0}
@item PhysLine
Default value: @code{0}
@item PhysSurface
Default value: @code{0}
@item Computation
Default value: @code{-1}
@item MinScale
Default value: @code{-1}
@item MaxScale
Default value: @code{-1}
@item Orthogonal
Default value: @code{-1}
@end table

@item Plugin(Divergence)
Plugin(Divergence) computes the divergence of the field in the view `View'.

If `View' < 0, the plugin is run on the current view.

Plugin(Divergence) creates one new view.
Numeric options:
@table @code
@item View
Default value: @code{-1}
@end table

@item Plugin(Eigenvalues)
Plugin(Eigenvalues) computes the three real eigenvalues of each tensor in the view `View'.

If `View' < 0, the plugin is run on the current view.

Plugin(Eigenvalues) creates three new scalar views.
Numeric options:
@table @code
@item View
Default value: @code{-1}
@end table

@item Plugin(Eigenvectors)
Plugin(Eigenvectors) computes the three (right) eigenvectors of each tensor in the view `View' and sorts them according to the value of the associated eigenvalues.

If `ScaleByEigenvalues' is set, each eigenvector is scaled by its associated eigenvalue. The plugin gives an error if the eigenvectors are complex.

If `View' < 0, the plugin is run on the current view.

Plugin(Eigenvectors) creates three new vector view.
Numeric options:
@table @code
@item ScaleByEigenvalues
Default value: @code{1}
@item View
Default value: @code{-1}
@end table

@item Plugin(ExtractEdges)
Plugin(ExtractEdges) extracts sharp edges from a triangular mesh.

Plugin(ExtractEdges) creates one new view.
Numeric options:
@table @code
@item Angle
Default value: @code{40}
@item IncludeBoundary
Default value: @code{1}
@end table

@item Plugin(ExtractElements)
Plugin(ExtractElements) extracts some elements from the view `View'. If `MinVal' != `MaxVal', it extracts the elements whose `TimeStep'-th values (averaged by element) are comprised between `MinVal' and `MaxVal'. If `Visible' != 0, it extracts visible elements. 

If `View' < 0, the plugin is run on the current view.

Plugin(ExtractElements) creates one new view.
Numeric options:
@table @code
@item MinVal
Default value: @code{0}
@item MaxVal
Default value: @code{0}
@item TimeStep
Default value: @code{0}
@item Visible
Default value: @code{1}
@item View
Default value: @code{-1}
@end table

@item Plugin(FieldFromAmplitudePhase)
Plugin(FieldFromAmplitudePhase) builds a complex field 'u' from amplitude 'a' (complex) and phase 'phi' given in two different 'Views' u = a * exp(k*phi), with k the wavenumber. 

The result is to be interpolated in a sufficiently fine mesh: 'MeshFile'. 

Plugin(FieldFromAmplitudePhase) generates one new view.
String options:
@table @code
@item MeshFile
Default value: @code{"fine.msh"}
@end table
Numeric options:
@table @code
@item Wavenumber
Default value: @code{5}
@item AmplitudeView
Default value: @code{0}
@item PhaseView
Default value: @code{1}
@end table

@item Plugin(GSHHS)
Plugin(GSHHS) read different kind of contour lines data and write a .geo file on the surface of a sphere (the Earth).

The principal application is to load GSHHS data
 (see http://www.soest.hawaii.edu/wessel/gshhs/gshhs.html).

Valid values for "Format" are:

- "gshhs": open GSHHS file

- "loops2": import 2D contour lines in simple text format:

NB_POINTS_IN_FIRST_LOOP FIRST_LOOP_IS_CLOSED
COORD1 COORD2
COORD1 COORD2
...    ...
NB_POINTS_IN_SECOND_LOOP SECOND_LOOP_IS_CLOSED
...

(LOOP_IS_CLOSED specifies if this coast line describes a closed curve (0=no, 1=yes)).

In the case of "loops2" format, you can specify the coordinate system used in the input file with the "Coordinate" option. Valid values are

- "lonlat" for longitude-latidute radian,

- "lonlat_degrees" for longitude-latitude degrees,

- "UTM" for universal transverse mercartor ("UTMZone" option should be specified)

- "cartesian" for full 3D coordinates

- "radius" specify the earth radius.

If the "iField" option is set, consecutive points closer than the value of the field iField (in meters) will not be added.

If "MinStraitsFactor" > 0 and if a field iField is provided, coastlines closer than MinStraitsFactor * field(IField) are merged and inner corners which form an angle < pi/3 are removed.

The output is always in stereographic coordinates, if the "WritePolarSphere" option is greater than 0, a sphere is added to the geo file.

WARNING: this plugin is still experimental and needs polishing and error-handling. In particular, it will probably crash if an inexistant field id is given or if the input/output cannot be open.
String options:
@table @code
@item InFileName
Default value: @code{"gshhs_c.b"}
@item OutFileName
Default value: @code{"earth.geo"}
@item Format
Default value: @code{"gshhs"}
@item Coordinate
Default value: @code{"cartesian"}
@end table
Numeric options:
@table @code
@item iField
Default value: @code{-1}
@item UTMZone
Default value: @code{0}
@item UTMEquatorialRadius
Default value: @code{6.37814e+06}
@item UTMPolarRadius
Default value: @code{6.35675e+06}
@item radius
Default value: @code{6.37101e+06}
@item WritePolarSphere
Default value: @code{1}
@item MinStraitsFactor
Default value: @code{1}
@end table

@item Plugin(Gradient)
Plugin(Gradient) computes the gradient of the field in the view `View'.

If `View' < 0, the plugin is run on the current view.

Plugin(Gradient) creates one new view.
Numeric options:
@table @code
@item View
Default value: @code{-1}
@end table

@item Plugin(HarmonicToTime)
Plugin(HarmonicToTime) takes the values in the time steps `RealPart' and `ImaginaryPart' of the view `View', and creates a new view containing

`View'[`RealPart'] * cos(p) - `View'[`ImaginaryPart'] * sin(p)

with p = 2*Pi*k/`NumSteps', k = 0, ..., `NumSteps'-1.

If `View' < 0, the plugin is run on the current view.

Plugin(HarmonicToTime) creates one new view.
Numeric options:
@table @code
@item RealPart
Default value: @code{0}
@item ImaginaryPart
Default value: @code{1}
@item NumSteps
Default value: @code{20}
@item View
Default value: @code{-1}
@end table

@item Plugin(Homology)
Plugin(Homology) computes ranks and basis elements of (relative) homology and cohomology spaces.

Define physical groups in order to specify the computation domain and the relative subdomain. Otherwise the whole mesh is the domain and the relative subdomain is empty. 

Plugin(Homology) creates new views, one for each basis element. The resulting basis chains together with the mesh are saved to the file given.
String options:
@table @code
@item Filename
Default value: @code{"homology.msh"}
@end table
Numeric options:
@table @code
@item PhysicalGroupForDomain1
Default value: @code{0}
@item PhysicalGroupForDomain2
Default value: @code{0}
@item PhysicalGroupForSubdomain1
Default value: @code{0}
@item PhysicalGroupForSubdomain2
Default value: @code{0}
@item CompututeHomology
Default value: @code{1}
@item ComputeCohomology
Default value: @code{0}
@end table

@item Plugin(Integrate)
Plugin(Integrate) integrates scalar fields over all the elements in the view `View', as well as the circulation/flux of vector fields over line/surface elements.

If `View' < 0, the plugin is run on the current view.

Plugin(Integrate) creates one new view.
Numeric options:
@table @code
@item View
Default value: @code{-1}
@end table

@item Plugin(Isosurface)
Plugin(Isosurface) extracts the isosurface of value `Value' from the view `View', and draws the `OtherTimeStep'-th step of the view `OtherView' on this isosurface.

If `ExtractVolume' is nonzero, the plugin extracts the isovolume with values greater (if `ExtractVolume' > 0) or smaller (if `ExtractVolume' < 0) than the isosurface `Value'.

If `OtherTimeStep' < 0, the plugin uses, for each time step in `View', the corresponding time step in `OtherView'. If `OtherView' < 0, the plugin uses `View' as the value source.

If `View' < 0, the plugin is run on the current view.

Plugin(Isosurface) creates as many views as there are time steps in `View'.
Numeric options:
@table @code
@item Value
Default value: @code{0}
@item ExtractVolume
Default value: @code{0}
@item RecurLevel
Default value: @code{4}
@item TargetError
Default value: @code{0}
@item View
Default value: @code{-1}
@item OtherTimeStep
Default value: @code{-1}
@item OtherView
Default value: @code{-1}
@end table

@item Plugin(Lambda2)
Plugin(Lambda2) computes the eigenvalues Lambda(1,2,3) of the tensor (S_ik S_kj + Om_ik Om_kj), where S_ij = 0.5 (ui,j + uj,i) and Om_ij = 0.5 (ui,j - uj,i) are respectively the symmetric and antisymmetric parts of the velocity gradient tensor.

Vortices are well represented by regions where Lambda(2) is negative.

If `View' contains tensor elements, the plugin directly uses the tensors as the values of the velocity gradient tensor; if `View' contains vector elements, the plugin uses them as the velocities from which to derive the velocity gradient tensor.

If `View' < 0, the plugin is run on the current view.

Plugin(Lambda2) creates one new view.
Numeric options:
@table @code
@item Eigenvalue
Default value: @code{2}
@item View
Default value: @code{-1}
@end table

@item Plugin(LongitudeLatitude)
Plugin(LongituteLatitude) projects the view `View' in longitude-latitude.

If `View' < 0, the plugin is run on the current view.

Plugin(LongituteLatitude) is executed in place.
Numeric options:
@table @code
@item View
Default value: @code{-1}
@end table

@item Plugin(MakeSimplex)
Plugin(MakeSimplex) decomposes all non-simplectic elements (quadrangles, prisms, hexahedra, pyramids) in the view `View' into simplices (triangles, tetrahedra).

If `View' < 0, the plugin is run on the current view.

Plugin(MakeSimplex) is executed in-place.
Numeric options:
@table @code
@item View
Default value: @code{-1}
@end table

@item Plugin(MathEval)
Plugin(MathEval) creates a new view using data from the time step `TimeStep' in the view `View'.

If only `Expression0' is given (and `Expression1', ..., `Expression8' are all empty), the plugin creates a scalar view. If `Expression0', `Expression1' and/or `Expression2' are given (and `Expression3', ..., `Expression8' are all empty) the plugin creates a vector view. Otherwise the plugin creates a tensor view.

In addition to the usual mathematical functions (Exp, Log, Sqrt, Sin, Cos, Fabs, etc.) and operators (+, -, *, /, ^), all expressions can contain:

- the symbols v0, v1, v2, ..., vn, which represent the n components in `View';

- the symbols w0, w1, w2, ..., wn, which represent the n components of `OtherView', at time step `OtherTimeStep';

- the symbols x, y and z, which represent the three spatial coordinates.

If `TimeStep' < 0, the plugin extracts data from all the time steps in the view.

If `View' < 0, the plugin is run on the current view.

Plugin(MathEval) creates one new view.If `PhysicalRegion' < 0, the plugin is runon all physical regions.

Plugin(MathEval) creates one new view.
String options:
@table @code
@item Expression0
Default value: @code{"Sqrt(v0^2+v1^2+v2^2)"}
@item Expression1
Default value: @code{""}
@item Expression2
Default value: @code{""}
@item Expression3
Default value: @code{""}
@item Expression4
Default value: @code{""}
@item Expression5
Default value: @code{""}
@item Expression6
Default value: @code{""}
@item Expression7
Default value: @code{""}
@item Expression8
Default value: @code{""}
@end table
Numeric options:
@table @code
@item TimeStep
Default value: @code{-1}
@item View
Default value: @code{-1}
@item OtherTimeStep
Default value: @code{-1}
@item OtherView
Default value: @code{-1}
@item ForceInterpolation
Default value: @code{0}
@item PhysicalRegion
Default value: @code{-1}
@end table

@item Plugin(MinMax)
Plugin(MinMax) computes the min/max of a view.

If `View' < 0, the plugin is run on the current view.

If `OverTime' = 1, calculates the min-max over space AND time

Plugin(MinMax) creates two new views.
Numeric options:
@table @code
@item View
Default value: @code{-1}
@item OverTime
Default value: @code{0}
@end table

@item Plugin(ModifyComponent)
Plugin(ModifyComponent) sets the `Component'-th component of the `TimeStep'-th time step in the view `View' to the expression `Expression'.

`Expression' can contain:

- the usual mathematical functions (Log, Sqrt, Sin, Cos, Fabs, ...) and operators (+, -, *, /, ^);

- the symbols x, y and z, to retrieve the coordinates of the current node;

- the symbols Time and TimeStep, to retrieve the current time and time step values;

- the symbol v, to retrieve the `Component'-th component of the field in `View' at the `TimeStep'-th time step;

- the symbols v0, v1, v2, ..., v8, to retrieve each component of the field in `View' at the `TimeStep'-th time step;

- the symbol w, to retrieve the `Component'-th component of the field in `OtherView' at the `OtherTimeStep'-th time step. If `OtherView' and `View' are based on different spatial grids, or if their data types are different, `OtherView' is interpolated onto `View';

- the symbols w0, w1, w2, ..., w8, to retrieve each component of the field in `OtherView' at the `OtherTimeStep'-th time step.

If `TimeStep' < 0, the plugin automatically loops over all the time steps in `View' and evaluates `Expression' for each one.

If `OtherTimeStep' < 0, the plugin uses `TimeStep' instead.

If `Component' < 0, the plugin automatically  ops
over all the components in the view and evaluates `Expression' for each one.

If `View' < 0, the plugin is run on the current view.

If `OtherView' < 0, the plugin uses `View' instead.

Plugin(ModifyComponent) is executed in-place.
String options:
@table @code
@item Expression
Default value: @code{"v0 * Sin(x)"}
@end table
Numeric options:
@table @code
@item Component
Default value: @code{-1}
@item TimeStep
Default value: @code{-1}
@item View
Default value: @code{-1}
@item OtherTimeStep
Default value: @code{-1}
@item OtherView
Default value: @code{-1}
@item ForceInterpolation
Default value: @code{0}
@end table

@item Plugin(ModulusPhase)
Plugin(ModulusPhase) interprets the time steps `realPart' and `imaginaryPart' in the view `View' as the real and imaginary parts of a complex field and replaces them with their corresponding modulus and phase.

If `View' < 0, the plugin is run on the current view.

Plugin(ModulusPhase) is executed in-place.
Numeric options:
@table @code
@item RealPart
Default value: @code{0}
@item ImaginaryPart
Default value: @code{1}
@item View
Default value: @code{-1}
@end table

@item Plugin(NearToFarField)
Plugin(NearToFarField) computes the far field pattern from the near electric and magnetic fields on a surface (regular grid) enclosing the radiating device (antenna).

Parameters: the wavenumber, the far field distance (radious) and angular discretisation, i.e. the number of divisions for phi in [0, 2*Pi] and theta in [0, Pi].

If `View' < 0, the plugin is run on the current view.

Plugin(NearToFarField) creates one new view.
Numeric options:
@table @code
@item Wavenumber
Default value: @code{1}
@item FarDistance
Default value: @code{1}
@item NumPointsPhi
Default value: @code{120}
@item NumPointsTheta
Default value: @code{60}
@item EView
Default value: @code{0}
@item HView
Default value: @code{1}
@item Normalize
Default value: @code{1}
@item dB
Default value: @code{1}
@end table

@item Plugin(NearestNeighbor)
Plugin(NearestNeighbor) computes the distance from each point in `View' to its nearest neighbor.

If `View' < 0, the plugin is run on the current view.

Plugin(NearestNeighbor) is executed in-place.
Numeric options:
@table @code
@item View
Default value: @code{-1}
@end table

@item Plugin(Particles)
Plugin(Particles) computes the trajectory of particules in the force field given by the `TimeStep'-th time step of a vector view `View'.

The plugin takes as input a grid defined by the 3 points (`X0',`Y0',`Z0') (origin), (`X1',`Y1',`Z1') (axis of U) and (`X2',`Y2',`Z2') (axis of V).

The number of particles along U and V that are to be transported is set with the options `NumPointsU' and `NumPointsV'. The equation

A2 * d^2X(t)/dt^2 + A1 * dX(t)/dt + A0 * X(t) = F

is then solved with the initial conditions X(t=0) chosen as the grid, dX/dt(t=0)=0, and with F interpolated from the vector view.

Time stepping is done using a Newmark scheme with step size `DT' and `MaxIter' maximum number of iterations.

If `View' < 0, the plugin is run on the current view.

Plugin(Particles) creates one new view containing multi-step vector points.
Numeric options:
@table @code
@item X0
Default value: @code{0}
@item Y0
Default value: @code{0}
@item Z0
Default value: @code{0}
@item X1
Default value: @code{1}
@item Y1
Default value: @code{0}
@item Z1
Default value: @code{0}
@item X2
Default value: @code{0}
@item Y2
Default value: @code{1}
@item Z2
Default value: @code{0}
@item NumPointsU
Default value: @code{10}
@item NumPointsV
Default value: @code{1}
@item A2
Default value: @code{1}
@item A1
Default value: @code{0}
@item A0
Default value: @code{0}
@item DT
Default value: @code{0.1}
@item MaxIter
Default value: @code{100}
@item TimeStep
Default value: @code{0}
@item View
Default value: @code{-1}
@end table

@item Plugin(Probe)
Plugin(Probe) gets the value of the view `View' at the point (`X',`Y',`Z').

If `View' < 0, the plugin is run on the current view.

Plugin(Probe) creates one new view.
Numeric options:
@table @code
@item X
Default value: @code{0}
@item Y
Default value: @code{0}
@item Z
Default value: @code{0}
@item View
Default value: @code{-1}
@end table

@item Plugin(Remove)
Plugin(Remove) removes the marked items from the view `View'.

If `View' < 0, the plugin is run on the current view.

Plugin(Remove) is executed in-place.
Numeric options:
@table @code
@item Text2D
Default value: @code{1}
@item Text3D
Default value: @code{1}
@item Points
Default value: @code{0}
@item Lines
Default value: @code{0}
@item Triangles
Default value: @code{0}
@item Quadrangles
Default value: @code{0}
@item Tetrahedra
Default value: @code{0}
@item Hexahedra
Default value: @code{0}
@item Prisms
Default value: @code{0}
@item Pyramids
Default value: @code{0}
@item Scalar
Default value: @code{1}
@item Vector
Default value: @code{1}
@item Tensor
Default value: @code{1}
@item View
Default value: @code{-1}
@end table

@item Plugin(Scal2Vec)
Plugin(Scal2Vec) converts the scalar fields of 'View X', 'View Y' and/or 'View Z' into a vectorial field. The new view 'Name NewView' contains it.

If the value of 'View X', 'View Y' or 'View Z' is -1, the value of the vectorial field in the corresponding direction is 0.
String options:
@table @code
@item Name NewView
Default value: @code{"NewView"}
@end table
Numeric options:
@table @code
@item View X
Default value: @code{-1}
@item View Y
Default value: @code{-1}
@item View Z
Default value: @code{-1}
@end table

@item Plugin(Skin)
Plugin(Skin) extracts the boundary (skin) of the view `View'. If `Visible' is set, the plugin only extracts the skin of visible entities.

If `View' < 0, the plugin is run on the current view.

Plugin(Skin) creates one new view.
Numeric options:
@table @code
@item Visible
Default value: @code{1}
@item View
Default value: @code{-1}
@end table

@item Plugin(Smooth)
Plugin(Smooth) averages the values at the nodes of the view `View'.

If `View' < 0, the plugin is run on the current view.

Plugin(Smooth) is executed in-place.
Numeric options:
@table @code
@item View
Default value: @code{-1}
@end table

@item Plugin(SphericalRaise)
Plugin(SphericalRaise) transforms the coordinates of the elements in the view `View' using the values associated with the `TimeStep'-th time step.

Instead of elevating the nodes along the X, Y and Z axes as with the View[`View'].RaiseX, View[`View'].RaiseY and View[`View'].RaiseZ options, the raise is applied along the radius of a sphere centered at (`Xc', `Yc', `Zc').

To produce a standard radiation pattern, set `Offset' to minus the radius of the sphere the original data lives on.

If `View' < 0, the plugin is run on the current view.

Plugin(SphericalRaise) is executed in-place.
Numeric options:
@table @code
@item Xc
Default value: @code{0}
@item Yc
Default value: @code{0}
@item Zc
Default value: @code{0}
@item Raise
Default value: @code{1}
@item Offset
Default value: @code{0}
@item TimeStep
Default value: @code{0}
@item View
Default value: @code{-1}
@end table

@item Plugin(StreamLines)
Plugin(StreamLines) computes stream lines from the `TimeStep'-th time step of a vector view `View' and optionally interpolates the scalar view `OtherView' on the resulting stream lines.

The plugin takes as input a grid defined by the 3 points (`X0',`Y0',`Z0') (origin), (`X1',`Y1',`Z1') (axis of U) and (`X2',`Y2',`Z2') (axis of V).

The number of points along U and V that are to be transported is set with the options `NumPointsU' and `NumPointsV'. The equation

dX(t)/dt = V(x,y,z)

is then solved with the initial condition X(t=0) chosen as the grid and with V(x,y,z) interpolated on the vector view.

The time stepping scheme is a RK44 with step size `DT' and `MaxIter' maximum number of iterations.

If `TimeStep' < 0, the plugin tries to compute streamlines of the unsteady flow.

If `View' < 0, the plugin is run on the current view.

Plugin(StreamLines) creates one new view. This view contains multi-step vector points if `OtherView' < 0, or single-step scalar lines if `OtherView' >= 0.
Numeric options:
@table @code
@item X0
Default value: @code{0}
@item Y0
Default value: @code{0}
@item Z0
Default value: @code{0}
@item X1
Default value: @code{1}
@item Y1
Default value: @code{0}
@item Z1
Default value: @code{0}
@item X2
Default value: @code{0}
@item Y2
Default value: @code{1}
@item Z2
Default value: @code{0}
@item NumPointsU
Default value: @code{10}
@item NumPointsV
Default value: @code{1}
@item DT
Default value: @code{0.1}
@item MaxIter
Default value: @code{100}
@item TimeStep
Default value: @code{0}
@item View
Default value: @code{-1}
@item OtherView
Default value: @code{-1}
@end table

@item Plugin(Tetrahedralize)
Plugin(Tetrahedralize) tetrahedralizes the points in the view `View'.

If `View' < 0, the plugin is run on the current view.

Plugin(Tetrahedralize) creates one new view.
Numeric options:
@table @code
@item View
Default value: @code{-1}
@end table

@item Plugin(Transform)
Plugin(Transform) transforms the homogeneous node coordinates (x,y,z,1) of the elements in the view `View' by the matrix

[`A11' `A12' `A13' `Tx']
[`A21' `A22' `A23' `Ty']
[`A31' `A32' `A33' `Tz'].

If `SwapOrientation' is set, the orientation of the elements is reversed.

If `View' < 0, the plugin is run on the current view.

Plugin(Transform) is executed in-place.
Numeric options:
@table @code
@item A11
Default value: @code{1}
@item A12
Default value: @code{0}
@item A13
Default value: @code{0}
@item A21
Default value: @code{0}
@item A22
Default value: @code{1}
@item A23
Default value: @code{0}
@item A31
Default value: @code{0}
@item A32
Default value: @code{0}
@item A33
Default value: @code{1}
@item Tx
Default value: @code{0}
@item Ty
Default value: @code{0}
@item Tz
Default value: @code{0}
@item SwapOrientation
Default value: @code{0}
@item View
Default value: @code{-1}
@end table

@item Plugin(Triangulate)
Plugin(Triangulate) triangulates the points in the view `View', assuming that all the points belong to a surface that can be projected one-to-one onto a plane.

If `View' < 0, the plugin is run on the current view.

Plugin(Triangulate) creates one new view.
Numeric options:
@table @code
@item View
Default value: @code{-1}
@end table

@item Plugin(Warp)
Plugin(Warp) transforms the elements in the view `View' by adding to their node coordinates the vector field stored in the `TimeStep'-th time step of the view `OtherView', scaled by `Factor'.

If `View' < 0, the plugin is run on the current view.

If `OtherView' < 0, the vector field is taken as the field of surface normals multiplied by the `TimeStep' value in `View'. (The smoothing of the surface normals is controlled by the `SmoothingAngle' parameter.)

Plugin(Warp) is executed in-place.
Numeric options:
@table @code
@item Factor
Default value: @code{1}
@item TimeStep
Default value: @code{0}
@item SmoothingAngle
Default value: @code{180}
@item View
Default value: @code{-1}
@item OtherView
Default value: @code{-1}
@end table

@end ftable
